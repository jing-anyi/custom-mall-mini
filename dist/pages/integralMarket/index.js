(wx["webpackJsonp"] = wx["webpackJsonp"] || []).push([["pages/IntegralMarket/index"],{

/***/ "./node_modules/@nutui/nutui-taro/dist/packages/_es/Card.js":
/*!******************************************************************!*\
  !*** ./node_modules/@nutui/nutui-taro/dist/packages/_es/Card.js ***!
  \******************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": function() { return /* binding */ index_taro; }
/* harmony export */ });
/* harmony import */ var _Users_jinganyi_CustomMallMini_node_modules_babel_runtime_helpers_esm_defineProperty_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/defineProperty.js */ "./node_modules/@babel/runtime/helpers/esm/defineProperty.js");
/* harmony import */ var _component_25dcca32_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./component-25dcca32.js */ "./node_modules/@nutui/nutui-taro/dist/packages/_es/component-25dcca32.js");
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/index.js");
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(vue__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _Price_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./Price.js */ "./node_modules/@nutui/nutui-taro/dist/packages/_es/Price.js");
/* harmony import */ var _Tag_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./Tag.js */ "./node_modules/@nutui/nutui-taro/dist/packages/_es/Tag.js");
/* harmony import */ var _plugin_vue_export_helper_cc2b3d55_js__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./_plugin-vue_export-helper-cc2b3d55.js */ "./node_modules/@nutui/nutui-taro/dist/packages/_es/_plugin-vue_export-helper-cc2b3d55.js");
/* harmony import */ var _nutui_icons_vue_taro__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @nutui/icons-vue-taro */ "./node_modules/@nutui/icons-vue-taro/dist/es/index.es.js");

var _components;







var _createComponent = (0,_component_25dcca32_js__WEBPACK_IMPORTED_MODULE_2__.c)("card"),
  create = _createComponent.create;
var _sfc_main = create({
  components: (_components = {}, (0,_Users_jinganyi_CustomMallMini_node_modules_babel_runtime_helpers_esm_defineProperty_js__WEBPACK_IMPORTED_MODULE_3__["default"])(_components, _Price_js__WEBPACK_IMPORTED_MODULE_4__["default"].name, _Price_js__WEBPACK_IMPORTED_MODULE_4__["default"]), (0,_Users_jinganyi_CustomMallMini_node_modules_babel_runtime_helpers_esm_defineProperty_js__WEBPACK_IMPORTED_MODULE_3__["default"])(_components, _Tag_js__WEBPACK_IMPORTED_MODULE_5__["default"].name, _Tag_js__WEBPACK_IMPORTED_MODULE_5__["default"]), _components),
  props: {
    imgUrl: {
      type: String,
      default: ""
    },
    title: {
      type: String,
      default: ""
    },
    price: {
      type: String,
      default: ""
    },
    vipPrice: {
      type: String,
      default: ""
    },
    shopDesc: {
      type: String,
      default: ""
    },
    delivery: {
      type: String,
      default: ""
    },
    shopName: {
      type: String,
      default: ""
    },
    isNeedPrice: {
      type: Boolean,
      default: true
    }
  },
  setup: function setup(props, _ref) {
    var emit = _ref.emit,
      slots = _ref.slots;
    var isHaveSlot = function isHaveSlot(slot) {
      return slots[slot];
    };
    return {
      isHaveSlot: isHaveSlot
    };
  }
});
var _hoisted_1 = {
  class: "nut-card"
};
var _hoisted_2 = {
  class: "nut-card__left"
};
var _hoisted_3 = ["src"];
var _hoisted_4 = {
  class: "nut-card__right"
};
var _hoisted_5 = {
  class: "nut-card__right__title"
};
var _hoisted_6 = {
  key: 0,
  class: "nut-card__right__price"
};
var _hoisted_7 = {
  class: "nut-card__right__other"
};
var _hoisted_8 = {
  class: "nut-card__right__shop"
};
var _hoisted_9 = {
  class: "nut-card__right__shop__name"
};
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  var _component_nut_price = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("nut-price");
  var _component_nut_tag = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("nut-tag");
  return (0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_1, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_2, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("img", {
    src: _ctx.imgUrl,
    alt: ""
  }, null, 8, _hoisted_3)]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createTextVNode)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_4, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_5, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(_ctx.title), 1), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createTextVNode)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.renderSlot)(_ctx.$slots, "prolist"), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createTextVNode)(), _ctx.isNeedPrice ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_6, [_ctx.isHaveSlot("price") ? (0,vue__WEBPACK_IMPORTED_MODULE_0__.renderSlot)(_ctx.$slots, "price", {
    key: 0
  }) : ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createBlock)(_component_nut_price, {
    key: 1,
    price: _ctx.price
  }, null, 8, ["price"])), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createTextVNode)(), _ctx.isHaveSlot("origin") ? (0,vue__WEBPACK_IMPORTED_MODULE_0__.renderSlot)(_ctx.$slots, "origin", {
    key: 2
  }) : ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createBlock)(_component_nut_price, {
    key: 3,
    class: "nut-card__right__price__origin",
    price: _ctx.vipPrice
  }, null, 8, ["price"])), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createTextVNode)(), _ctx.isHaveSlot("tag") ? (0,vue__WEBPACK_IMPORTED_MODULE_0__.renderSlot)(_ctx.$slots, "tag", {
    key: 4
  }) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("", true)])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("", true), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createTextVNode)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_7, [_ctx.isHaveSlot("shop-tag") ? (0,vue__WEBPACK_IMPORTED_MODULE_0__.renderSlot)(_ctx.$slots, "shop-tag", {
    key: 0
  }) : ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)(vue__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
    key: 1
  }, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_nut_tag, {
    type: "danger"
  }, {
    default: (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(function () {
      return [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createTextVNode)((0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(_ctx.shopDesc), 1)];
    }),
    _: 1
  }), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createTextVNode)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_nut_tag, {
    plain: ""
  }, {
    default: (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(function () {
      return [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createTextVNode)((0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(_ctx.delivery), 1)];
    }),
    _: 1
  })], 64))]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createTextVNode)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_8, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_9, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(_ctx.shopName), 1), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createTextVNode)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.renderSlot)(_ctx.$slots, "footer")])])]);
}
var index_taro = /* @__PURE__ */(0,_plugin_vue_export_helper_cc2b3d55_js__WEBPACK_IMPORTED_MODULE_6__._)(_sfc_main, [["render", _sfc_render]]);


/***/ }),

/***/ "./node_modules/@nutui/nutui-taro/dist/packages/_es/Tag.js":
/*!*****************************************************************!*\
  !*** ./node_modules/@nutui/nutui-taro/dist/packages/_es/Tag.js ***!
  \*****************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": function() { return /* binding */ Tag; }
/* harmony export */ });
/* harmony import */ var _Users_jinganyi_CustomMallMini_node_modules_babel_runtime_helpers_esm_defineProperty_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/defineProperty.js */ "./node_modules/@babel/runtime/helpers/esm/defineProperty.js");
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/index.js");
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(vue__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _nutui_icons_vue_taro__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @nutui/icons-vue-taro */ "./node_modules/@nutui/icons-vue-taro/dist/es/index.es.js");
/* harmony import */ var _component_25dcca32_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./component-25dcca32.js */ "./node_modules/@nutui/nutui-taro/dist/packages/_es/component-25dcca32.js");
/* harmony import */ var _plugin_vue_export_helper_cc2b3d55_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./_plugin-vue_export-helper-cc2b3d55.js */ "./node_modules/@nutui/nutui-taro/dist/packages/_es/_plugin-vue_export-helper-cc2b3d55.js");






var _createComponent = (0,_component_25dcca32_js__WEBPACK_IMPORTED_MODULE_2__.c)("tag"),
  componentName = _createComponent.componentName,
  create = _createComponent.create;
var _sfc_main = create({
  props: {
    color: {
      type: String,
      default: ""
    },
    textColor: {
      type: String,
      default: ""
    },
    type: {
      type: String,
      default: "default"
    },
    plain: {
      type: Boolean,
      default: false
    },
    round: {
      type: Boolean,
      default: false
    },
    mark: {
      type: Boolean,
      default: false
    },
    closeable: {
      type: Boolean,
      default: false
    }
  },
  components: {
    Close: _nutui_icons_vue_taro__WEBPACK_IMPORTED_MODULE_1__.Close
  },
  emits: ["close", "click"],
  setup: function setup(props, _ref) {
    var emit = _ref.emit;
    var _toRefs = (0,vue__WEBPACK_IMPORTED_MODULE_0__.toRefs)(props),
      type = _toRefs.type,
      color = _toRefs.color,
      plain = _toRefs.plain,
      round = _toRefs.round,
      mark = _toRefs.mark,
      textColor = _toRefs.textColor;
    var classes = (0,vue__WEBPACK_IMPORTED_MODULE_0__.computed)(function () {
      var _ref2;
      var prefixCls = componentName;
      return _ref2 = {}, (0,_Users_jinganyi_CustomMallMini_node_modules_babel_runtime_helpers_esm_defineProperty_js__WEBPACK_IMPORTED_MODULE_3__["default"])(_ref2, prefixCls, true), (0,_Users_jinganyi_CustomMallMini_node_modules_babel_runtime_helpers_esm_defineProperty_js__WEBPACK_IMPORTED_MODULE_3__["default"])(_ref2, "".concat(prefixCls, "--").concat(type.value), type.value), (0,_Users_jinganyi_CustomMallMini_node_modules_babel_runtime_helpers_esm_defineProperty_js__WEBPACK_IMPORTED_MODULE_3__["default"])(_ref2, "".concat(prefixCls, "--plain"), plain.value), (0,_Users_jinganyi_CustomMallMini_node_modules_babel_runtime_helpers_esm_defineProperty_js__WEBPACK_IMPORTED_MODULE_3__["default"])(_ref2, "".concat(prefixCls, "--round"), round.value), (0,_Users_jinganyi_CustomMallMini_node_modules_babel_runtime_helpers_esm_defineProperty_js__WEBPACK_IMPORTED_MODULE_3__["default"])(_ref2, "".concat(prefixCls, "--mark"), mark.value), _ref2;
    });
    var getStyle = function getStyle() {
      var style = {};
      if (textColor.value) {
        style.color = textColor.value;
      } else if (color.value && plain.value) {
        style.color = color.value;
      }
      if (plain.value) {
        style.background = "#fff";
        style["border-color"] = color.value;
      } else if (color.value) {
        style.background = color.value;
      }
      return style;
    };
    var onClose = function onClose(event) {
      event.stopPropagation();
      emit("close", event);
    };
    var onClick = function onClick(event) {
      emit("click", event);
    };
    return {
      classes: classes,
      getStyle: getStyle,
      onClose: onClose,
      onClick: onClick
    };
  }
});
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  var _component_Close = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("Close");
  return (0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("view", {
    class: (0,vue__WEBPACK_IMPORTED_MODULE_0__.normalizeClass)(_ctx.classes),
    style: (0,vue__WEBPACK_IMPORTED_MODULE_0__.normalizeStyle)(_ctx.getStyle()),
    onClick: _cache[0] || (_cache[0] = function () {
      return _ctx.onClick && _ctx.onClick.apply(_ctx, arguments);
    })
  }, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.renderSlot)(_ctx.$slots, "default"), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createTextVNode)(), _ctx.closeable ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createBlock)(_component_Close, {
    key: 0,
    class: "nut-tag--close",
    size: "11",
    onClick: _ctx.onClose
  }, null, 8, ["onClick"])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("", true)], 6);
}
var Tag = /* @__PURE__ */(0,_plugin_vue_export_helper_cc2b3d55_js__WEBPACK_IMPORTED_MODULE_4__._)(_sfc_main, [["render", _sfc_render]]);


/***/ }),

/***/ "./node_modules/@nutui/nutui-taro/dist/packages/card/index.scss":
/*!**********************************************************************!*\
  !*** ./node_modules/@nutui/nutui-taro/dist/packages/card/index.scss ***!
  \**********************************************************************/
/***/ (function() {

"use strict";
// extracted by mini-css-extract-plugin


/***/ }),

/***/ "./node_modules/@nutui/nutui-taro/dist/packages/tag/index.scss":
/*!*********************************************************************!*\
  !*** ./node_modules/@nutui/nutui-taro/dist/packages/tag/index.scss ***!
  \*********************************************************************/
/***/ (function() {

"use strict";
// extracted by mini-css-extract-plugin


/***/ }),

/***/ "./node_modules/@tarojs/taro-loader/lib/raw.js!./src/pages/IntegralMarket/index.vue":
/*!******************************************************************************************!*\
  !*** ./node_modules/@tarojs/taro-loader/lib/raw.js!./src/pages/IntegralMarket/index.vue ***!
  \******************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _index_vue_vue_type_template_id_a25053a2__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./index.vue?vue&type=template&id=a25053a2 */ "./src/pages/IntegralMarket/index.vue?vue&type=template&id=a25053a2");
/* harmony import */ var _index_vue_vue_type_script_lang_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./index.vue?vue&type=script&lang=js */ "./src/pages/IntegralMarket/index.vue?vue&type=script&lang=js");
/* harmony import */ var _Users_jinganyi_CustomMallMini_node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/vue-loader/dist/exportHelper.js */ "./node_modules/vue-loader/dist/exportHelper.js");
/* unplugin-vue-components disabled */



;
const __exports__ = /*#__PURE__*/(0,_Users_jinganyi_CustomMallMini_node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_2__["default"])(_index_vue_vue_type_script_lang_js__WEBPACK_IMPORTED_MODULE_1__["default"], [['render',_index_vue_vue_type_template_id_a25053a2__WEBPACK_IMPORTED_MODULE_0__.render],['__file',"src/pages/IntegralMarket/index.vue"]])
/* hot reload */
if (false) {}


/* harmony default export */ __webpack_exports__["default"] = (__exports__);

/***/ }),

/***/ "./node_modules/unplugin/dist/webpack/loaders/transform.js?unpluginName=unplugin-vue-components!./node_modules/babel-loader/lib/index.js??clonedRuleSet-11.use[0]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/IntegralMarket/index.vue?vue&type=script&lang=js":
/*!************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/unplugin/dist/webpack/loaders/transform.js?unpluginName=unplugin-vue-components!./node_modules/babel-loader/lib/index.js??clonedRuleSet-11.use[0]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/IntegralMarket/index.vue?vue&type=script&lang=js ***!
  \************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _Users_jinganyi_CustomMallMini_node_modules_babel_runtime_helpers_esm_objectSpread2_js__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/objectSpread2.js */ "./node_modules/@babel/runtime/helpers/esm/objectSpread2.js");
/* harmony import */ var _Users_jinganyi_CustomMallMini_node_modules_babel_runtime_helpers_esm_regeneratorRuntime_js__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/regeneratorRuntime.js */ "./node_modules/@babel/runtime/helpers/esm/regeneratorRuntime.js");
/* harmony import */ var _Users_jinganyi_CustomMallMini_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js */ "./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js");
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/index.js");
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(vue__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _utils_api_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../utils/api.js */ "./src/pages/utils/api.js");
/* harmony import */ var _nutui_icons_vue_taro__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @nutui/icons-vue-taro */ "./node_modules/@nutui/icons-vue-taro/dist/es/index.es.js");
/* harmony import */ var _tarojs_taro__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @tarojs/taro */ "./node_modules/@tarojs/taro/index.js");
/* harmony import */ var _tarojs_taro__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_tarojs_taro__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var currency_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! currency.js */ "./node_modules/currency.js/dist/currency.min.js");
/* harmony import */ var currency_js__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(currency_js__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var fs__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! fs */ "?9d9a");
/* harmony import */ var fs__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(fs__WEBPACK_IMPORTED_MODULE_5__);
/* unplugin-vue-components disabled */









/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'Index',
  components: {
    Del: _nutui_icons_vue_taro__WEBPACK_IMPORTED_MODULE_2__.Del,
    Edit: _nutui_icons_vue_taro__WEBPACK_IMPORTED_MODULE_2__.Edit,
    Checklist: _nutui_icons_vue_taro__WEBPACK_IMPORTED_MODULE_2__.Checklist
  },
  setup: function setup() {
    var state = (0,vue__WEBPACK_IMPORTED_MODULE_0__.reactive)({
      integralMarketList: [],
      imgUrl: '//img10.360buyimg.com/n2/s240x240_jfs/t1/210890/22/4728/163829/6163a590Eb7c6f4b5/6390526d49791cb9.jpg!q70.jpg',
      title: '活蟹】湖塘煙雨 阳澄湖大闸蟹公4.5两 母3.5两 4对8只 鲜活生鲜螃蟹现货水产礼盒海鲜水',
      price: '388',
      vipPrice: '378',
      shopDesc: '自营',
      delivery: '厂商配送',
      shopName: '阳澄湖大闸蟹自营店>'
    });
    var GetIntegralMarket = /*#__PURE__*/function () {
      var _ref = (0,_Users_jinganyi_CustomMallMini_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_6__["default"])( /*#__PURE__*/(0,_Users_jinganyi_CustomMallMini_node_modules_babel_runtime_helpers_esm_regeneratorRuntime_js__WEBPACK_IMPORTED_MODULE_7__["default"])().mark(function _callee() {
        var integralMarketRes;
        return (0,_Users_jinganyi_CustomMallMini_node_modules_babel_runtime_helpers_esm_regeneratorRuntime_js__WEBPACK_IMPORTED_MODULE_7__["default"])().wrap(function _callee$(_context) {
          while (1) switch (_context.prev = _context.next) {
            case 0:
              _context.next = 2;
              return (0,_utils_api_js__WEBPACK_IMPORTED_MODULE_1__.GetIntegralMarketList)({});
            case 2:
              integralMarketRes = _context.sent;
              if (integralMarketRes.code == 200) {
                state.integralMarketList = integralMarketRes.data.list;
              }
            case 4:
            case "end":
              return _context.stop();
          }
        }, _callee);
      }));
      return function GetIntegralMarket() {
        return _ref.apply(this, arguments);
      };
    }();
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.onMounted)(function () {
      GetIntegralMarket();
    });
    // 购买
    var GoBuy = function GoBuy(tarVal) {
      _tarojs_taro__WEBPACK_IMPORTED_MODULE_3___default().navigateTo({
        url: "/pages/GoodsCloseAnAccount/index?entryType=3&commodityId=".concat(tarVal.id)
      });
    };
    return (0,_Users_jinganyi_CustomMallMini_node_modules_babel_runtime_helpers_esm_objectSpread2_js__WEBPACK_IMPORTED_MODULE_8__["default"])({
      GoBuy: GoBuy
    }, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toRefs)(state));
  }
});

/***/ }),

/***/ "./node_modules/unplugin/dist/webpack/loaders/transform.js?unpluginName=unplugin-vue-components!./node_modules/babel-loader/lib/index.js??clonedRuleSet-11.use[0]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[3]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/IntegralMarket/index.vue?vue&type=template&id=a25053a2":
/*!****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/unplugin/dist/webpack/loaders/transform.js?unpluginName=unplugin-vue-components!./node_modules/babel-loader/lib/index.js??clonedRuleSet-11.use[0]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[3]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/IntegralMarket/index.vue?vue&type=template&id=a25053a2 ***!
  \****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": function() { return /* binding */ render; }
/* harmony export */ });
/* harmony import */ var _nutui_nutui_taro__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @nutui/nutui-taro */ "./node_modules/@nutui/nutui-taro/dist/packages/card/index.mjs");
/* harmony import */ var _nutui_nutui_taro_dist_packages_card_style__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @nutui/nutui-taro/dist/packages/card/style */ "./node_modules/@nutui/nutui-taro/dist/packages/card/style.mjs");
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vue */ "./node_modules/vue/index.js");
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(vue__WEBPACK_IMPORTED_MODULE_1__);
/* unplugin-vue-components disabled */

var _hoisted_1 = {
  class: "integralMarket"
};
var _hoisted_2 = {
  class: "content"
};
var _hoisted_3 = {
  class: "item-integral"
};
function render(_ctx, _cache, $props, $setup, $data, $options) {
  var _component_nut_card = _nutui_nutui_taro__WEBPACK_IMPORTED_MODULE_2__.Card;
  return (0,vue__WEBPACK_IMPORTED_MODULE_1__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_1__.createElementBlock)("div", _hoisted_1, [(0,vue__WEBPACK_IMPORTED_MODULE_1__.createElementVNode)("div", _hoisted_2, [((0,vue__WEBPACK_IMPORTED_MODULE_1__.openBlock)(true), (0,vue__WEBPACK_IMPORTED_MODULE_1__.createElementBlock)(vue__WEBPACK_IMPORTED_MODULE_1__.Fragment, null, (0,vue__WEBPACK_IMPORTED_MODULE_1__.renderList)(_ctx.integralMarketList, function (i, idx) {
    return (0,vue__WEBPACK_IMPORTED_MODULE_1__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_1__.createBlock)(_component_nut_card, {
      key: idx,
      "img-url": i.bannerImgs[0],
      title: i.name + " | " + i.header,
      "is-need-price": false,
      class: "content-item",
      onClick: function onClick($event) {
        return $setup.GoBuy(i);
      }
    }, {
      "shop-tag": (0,vue__WEBPACK_IMPORTED_MODULE_1__.withCtx)(function () {
        return [(0,vue__WEBPACK_IMPORTED_MODULE_1__.createElementVNode)("div", _hoisted_3, (0,vue__WEBPACK_IMPORTED_MODULE_1__.toDisplayString)(i.price) + "积分", 1 /* TEXT */)];
      }),

      _: 2 /* DYNAMIC */
    }, 1032 /* PROPS, DYNAMIC_SLOTS */, ["img-url", "title", "onClick"]);
  }), 128 /* KEYED_FRAGMENT */))])]);
}

/***/ }),

/***/ "./src/pages/IntegralMarket/index.vue":
/*!********************************************!*\
  !*** ./src/pages/IntegralMarket/index.vue ***!
  \********************************************/
/***/ (function(__unused_webpack_module, __unused_webpack___webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _tarojs_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @tarojs/runtime */ "./node_modules/@tarojs/runtime/dist/runtime.esm.js");
/* harmony import */ var _node_modules_tarojs_taro_loader_lib_raw_js_index_vue__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../node_modules/@tarojs/taro-loader/lib/raw.js!./index.vue */ "./node_modules/@tarojs/taro-loader/lib/raw.js!./src/pages/IntegralMarket/index.vue");


var config = {"navigationBarTitleText":"积分商城"};


var inst = Page((0,_tarojs_runtime__WEBPACK_IMPORTED_MODULE_0__.createPageConfig)(_node_modules_tarojs_taro_loader_lib_raw_js_index_vue__WEBPACK_IMPORTED_MODULE_1__["default"], 'pages/IntegralMarket/index', {root:{cn:[]}}, config || {}))


/* unused harmony default export */ var __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_tarojs_taro_loader_lib_raw_js_index_vue__WEBPACK_IMPORTED_MODULE_1__["default"]);


/***/ }),

/***/ "./src/pages/IntegralMarket/index.vue?vue&type=script&lang=js":
/*!********************************************************************!*\
  !*** ./src/pages/IntegralMarket/index.vue?vue&type=script&lang=js ***!
  \********************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": function() { return /* reexport safe */ _node_modules_unplugin_dist_webpack_loaders_transform_js_unpluginName_unplugin_vue_components_node_modules_babel_loader_lib_index_js_clonedRuleSet_11_use_0_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_index_vue_vue_type_script_lang_js__WEBPACK_IMPORTED_MODULE_0__["default"]; }
/* harmony export */ });
/* harmony import */ var _node_modules_unplugin_dist_webpack_loaders_transform_js_unpluginName_unplugin_vue_components_node_modules_babel_loader_lib_index_js_clonedRuleSet_11_use_0_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_index_vue_vue_type_script_lang_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/unplugin/dist/webpack/loaders/transform.js?unpluginName=unplugin-vue-components!../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-11.use[0]!../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./index.vue?vue&type=script&lang=js */ "./node_modules/unplugin/dist/webpack/loaders/transform.js?unpluginName=unplugin-vue-components!./node_modules/babel-loader/lib/index.js??clonedRuleSet-11.use[0]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/IntegralMarket/index.vue?vue&type=script&lang=js");
/* unplugin-vue-components disabled */ 

/***/ }),

/***/ "./src/pages/IntegralMarket/index.vue?vue&type=template&id=a25053a2":
/*!**************************************************************************!*\
  !*** ./src/pages/IntegralMarket/index.vue?vue&type=template&id=a25053a2 ***!
  \**************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": function() { return /* reexport safe */ _node_modules_unplugin_dist_webpack_loaders_transform_js_unpluginName_unplugin_vue_components_node_modules_babel_loader_lib_index_js_clonedRuleSet_11_use_0_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_3_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_index_vue_vue_type_template_id_a25053a2__WEBPACK_IMPORTED_MODULE_0__.render; }
/* harmony export */ });
/* harmony import */ var _node_modules_unplugin_dist_webpack_loaders_transform_js_unpluginName_unplugin_vue_components_node_modules_babel_loader_lib_index_js_clonedRuleSet_11_use_0_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_3_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_index_vue_vue_type_template_id_a25053a2__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/unplugin/dist/webpack/loaders/transform.js?unpluginName=unplugin-vue-components!../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-11.use[0]!../../../node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[3]!../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./index.vue?vue&type=template&id=a25053a2 */ "./node_modules/unplugin/dist/webpack/loaders/transform.js?unpluginName=unplugin-vue-components!./node_modules/babel-loader/lib/index.js??clonedRuleSet-11.use[0]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[3]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/IntegralMarket/index.vue?vue&type=template&id=a25053a2");
/* unplugin-vue-components disabled */

/***/ }),

/***/ "?9d9a":
/*!********************!*\
  !*** fs (ignored) ***!
  \********************/
/***/ (function() {

/* (ignored) */

/***/ }),

/***/ "./node_modules/@nutui/nutui-taro/dist/packages/card/index.mjs":
/*!*********************************************************************!*\
  !*** ./node_modules/@nutui/nutui-taro/dist/packages/card/index.mjs ***!
  \*********************************************************************/
/***/ (function(__unused_webpack___webpack_module__, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Card": function() { return /* binding */ Card; }
/* harmony export */ });
/* harmony import */ var _es_Card_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../_es/Card.js */ "./node_modules/@nutui/nutui-taro/dist/packages/_es/Card.js");

const treeshaking = (t) => t;
const Card = treeshaking(_es_Card_js__WEBPACK_IMPORTED_MODULE_0__["default"]);


/***/ }),

/***/ "./node_modules/@nutui/nutui-taro/dist/packages/card/style.mjs":
/*!*********************************************************************!*\
  !*** ./node_modules/@nutui/nutui-taro/dist/packages/card/style.mjs ***!
  \*********************************************************************/
/***/ (function(__unused_webpack___webpack_module__, __unused_webpack___webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _styles_reset_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../styles/reset.css */ "./node_modules/@nutui/nutui-taro/dist/styles/reset.css");
/* harmony import */ var _tag_index_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../tag/index.scss */ "./node_modules/@nutui/nutui-taro/dist/packages/tag/index.scss");
/* harmony import */ var _price_index_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../price/index.scss */ "./node_modules/@nutui/nutui-taro/dist/packages/price/index.scss");
/* harmony import */ var _index_scss__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./index.scss */ "./node_modules/@nutui/nutui-taro/dist/packages/card/index.scss");






/***/ })

},
/******/ function(__webpack_require__) { // webpackRuntimeModules
/******/ var __webpack_exec__ = function(moduleId) { return __webpack_require__(__webpack_require__.s = moduleId); }
/******/ __webpack_require__.O(0, ["taro","vendors","common"], function() { return __webpack_exec__("./src/pages/IntegralMarket/index.vue"); });
/******/ var __webpack_exports__ = __webpack_require__.O();
/******/ }
]);
//# sourceMappingURL=index.js.map