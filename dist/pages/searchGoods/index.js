"use strict";
(wx["webpackJsonp"] = wx["webpackJsonp"] || []).push([["pages/SearchGoods/index"],{

/***/ "./node_modules/@tarojs/taro-loader/lib/raw.js!./src/pages/SearchGoods/index.vue":
/*!***************************************************************************************!*\
  !*** ./node_modules/@tarojs/taro-loader/lib/raw.js!./src/pages/SearchGoods/index.vue ***!
  \***************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

/* harmony import */ var _index_vue_vue_type_template_id_433b3f9d__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./index.vue?vue&type=template&id=433b3f9d */ "./src/pages/SearchGoods/index.vue?vue&type=template&id=433b3f9d");
/* harmony import */ var _index_vue_vue_type_script_lang_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./index.vue?vue&type=script&lang=js */ "./src/pages/SearchGoods/index.vue?vue&type=script&lang=js");
/* harmony import */ var _Users_jinganyi_CustomMallMini_node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/vue-loader/dist/exportHelper.js */ "./node_modules/vue-loader/dist/exportHelper.js");
/* unplugin-vue-components disabled */



;
const __exports__ = /*#__PURE__*/(0,_Users_jinganyi_CustomMallMini_node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_2__["default"])(_index_vue_vue_type_script_lang_js__WEBPACK_IMPORTED_MODULE_1__["default"], [['render',_index_vue_vue_type_template_id_433b3f9d__WEBPACK_IMPORTED_MODULE_0__.render],['__file',"src/pages/SearchGoods/index.vue"]])
/* hot reload */
if (false) {}


/* harmony default export */ __webpack_exports__["default"] = (__exports__);

/***/ }),

/***/ "./node_modules/unplugin/dist/webpack/loaders/transform.js?unpluginName=unplugin-vue-components!./node_modules/babel-loader/lib/index.js??clonedRuleSet-11.use[0]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/SearchGoods/index.vue?vue&type=script&lang=js":
/*!*********************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/unplugin/dist/webpack/loaders/transform.js?unpluginName=unplugin-vue-components!./node_modules/babel-loader/lib/index.js??clonedRuleSet-11.use[0]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/SearchGoods/index.vue?vue&type=script&lang=js ***!
  \*********************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

/* harmony import */ var _Users_jinganyi_CustomMallMini_node_modules_babel_runtime_helpers_esm_objectSpread2_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/objectSpread2.js */ "./node_modules/@babel/runtime/helpers/esm/objectSpread2.js");
/* harmony import */ var _Users_jinganyi_CustomMallMini_node_modules_babel_runtime_helpers_esm_regeneratorRuntime_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/regeneratorRuntime.js */ "./node_modules/@babel/runtime/helpers/esm/regeneratorRuntime.js");
/* harmony import */ var _Users_jinganyi_CustomMallMini_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js */ "./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js");
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/index.js");
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(vue__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _utils_api_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../utils/api.js */ "./src/pages/utils/api.js");
/* harmony import */ var _tarojs_taro__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @tarojs/taro */ "./node_modules/@tarojs/taro/index.js");
/* harmony import */ var _tarojs_taro__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_tarojs_taro__WEBPACK_IMPORTED_MODULE_2__);
/* unplugin-vue-components disabled */






/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'searchGoods',
  components: {},
  setup: function setup() {
    var state = (0,vue__WEBPACK_IMPORTED_MODULE_0__.reactive)({
      searchValue: '',
      tagList: [],
      searchResultList: []
    });
    var GetTag = /*#__PURE__*/function () {
      var _ref = (0,_Users_jinganyi_CustomMallMini_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_3__["default"])( /*#__PURE__*/(0,_Users_jinganyi_CustomMallMini_node_modules_babel_runtime_helpers_esm_regeneratorRuntime_js__WEBPACK_IMPORTED_MODULE_4__["default"])().mark(function _callee() {
        var tagRes;
        return (0,_Users_jinganyi_CustomMallMini_node_modules_babel_runtime_helpers_esm_regeneratorRuntime_js__WEBPACK_IMPORTED_MODULE_4__["default"])().wrap(function _callee$(_context) {
          while (1) switch (_context.prev = _context.next) {
            case 0:
              _context.next = 2;
              return (0,_utils_api_js__WEBPACK_IMPORTED_MODULE_1__.GetTagList)({});
            case 2:
              tagRes = _context.sent;
              if (tagRes.code == 200) {
                state.tagList = tagRes.data.list;
              }
            case 4:
            case "end":
              return _context.stop();
          }
        }, _callee);
      }));
      return function GetTag() {
        return _ref.apply(this, arguments);
      };
    }();
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.onMounted)(function () {
      GetTag();
    });
    var SearchClick = /*#__PURE__*/function () {
      var _ref2 = (0,_Users_jinganyi_CustomMallMini_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_3__["default"])( /*#__PURE__*/(0,_Users_jinganyi_CustomMallMini_node_modules_babel_runtime_helpers_esm_regeneratorRuntime_js__WEBPACK_IMPORTED_MODULE_4__["default"])().mark(function _callee2() {
        var resultObj, searchRes;
        return (0,_Users_jinganyi_CustomMallMini_node_modules_babel_runtime_helpers_esm_regeneratorRuntime_js__WEBPACK_IMPORTED_MODULE_4__["default"])().wrap(function _callee2$(_context2) {
          while (1) switch (_context2.prev = _context2.next) {
            case 0:
              resultObj = state.tagList.find(function (i) {
                return i.name.includes(state.searchValue);
              });
              _context2.next = 3;
              return (0,_utils_api_js__WEBPACK_IMPORTED_MODULE_1__.SearchGoods)({
                name: state.searchValue,
                categoryId: resultObj == undefined ? undefined : resultObj.code
              });
            case 3:
              searchRes = _context2.sent;
              if (searchRes.code == 200) {
                state.searchResultList = searchRes.data.list;
              }
            case 5:
            case "end":
              return _context2.stop();
          }
        }, _callee2);
      }));
      return function SearchClick() {
        return _ref2.apply(this, arguments);
      };
    }();
    var SearchClear = function SearchClear() {
      state.searchResultList = [];
    };
    var GoGoodsDetails = function GoGoodsDetails(tarGoodsVal) {
      _tarojs_taro__WEBPACK_IMPORTED_MODULE_2___default().navigateTo({
        url: "/pages/commodity-details/index?commodityId=".concat(tarGoodsVal.id)
      });
    };
    return (0,_Users_jinganyi_CustomMallMini_node_modules_babel_runtime_helpers_esm_objectSpread2_js__WEBPACK_IMPORTED_MODULE_5__["default"])({
      SearchClick: SearchClick,
      SearchClear: SearchClear,
      GoGoodsDetails: GoGoodsDetails
    }, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toRefs)(state));
  }
});

/***/ }),

/***/ "./node_modules/unplugin/dist/webpack/loaders/transform.js?unpluginName=unplugin-vue-components!./node_modules/babel-loader/lib/index.js??clonedRuleSet-11.use[0]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[3]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/SearchGoods/index.vue?vue&type=template&id=433b3f9d":
/*!*************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/unplugin/dist/webpack/loaders/transform.js?unpluginName=unplugin-vue-components!./node_modules/babel-loader/lib/index.js??clonedRuleSet-11.use[0]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[3]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/SearchGoods/index.vue?vue&type=template&id=433b3f9d ***!
  \*************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": function() { return /* binding */ render; }
/* harmony export */ });
/* harmony import */ var _nutui_nutui_taro__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @nutui/nutui-taro */ "./node_modules/@nutui/nutui-taro/dist/packages/empty/index.mjs");
/* harmony import */ var _nutui_nutui_taro_dist_packages_empty_style__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @nutui/nutui-taro/dist/packages/empty/style */ "./node_modules/@nutui/nutui-taro/dist/packages/empty/style.mjs");
/* harmony import */ var _nutui_nutui_taro__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @nutui/nutui-taro */ "./node_modules/@nutui/nutui-taro/dist/packages/price/index.mjs");
/* harmony import */ var _nutui_nutui_taro_dist_packages_price_style__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @nutui/nutui-taro/dist/packages/price/style */ "./node_modules/@nutui/nutui-taro/dist/packages/price/style.mjs");
/* harmony import */ var _nutui_nutui_taro__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @nutui/nutui-taro */ "./node_modules/@nutui/nutui-taro/dist/packages/searchbar/index.mjs");
/* harmony import */ var _nutui_nutui_taro_dist_packages_searchbar_style__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @nutui/nutui-taro/dist/packages/searchbar/style */ "./node_modules/@nutui/nutui-taro/dist/packages/searchbar/style.mjs");
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! vue */ "./node_modules/vue/index.js");
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(vue__WEBPACK_IMPORTED_MODULE_3__);
/* unplugin-vue-components disabled */



var _hoisted_1 = {
  class: "SearchGoods"
};
var _hoisted_2 = {
  key: 0,
  class: "content"
};
var _hoisted_3 = ["onClick"];
var _hoisted_4 = {
  class: "goods-photo"
};
var _hoisted_5 = ["src"];
var _hoisted_6 = {
  class: "goods-message"
};
var _hoisted_7 = {
  class: "goods-priceAndNum"
};
function render(_ctx, _cache, $props, $setup, $data, $options) {
  var _component_nut_searchbar = _nutui_nutui_taro__WEBPACK_IMPORTED_MODULE_4__.Searchbar;
  var _component_nut_price = _nutui_nutui_taro__WEBPACK_IMPORTED_MODULE_5__.Price;
  var _component_nut_empty = _nutui_nutui_taro__WEBPACK_IMPORTED_MODULE_6__.Empty;
  return (0,vue__WEBPACK_IMPORTED_MODULE_3__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_3__.createElementBlock)("div", _hoisted_1, [(0,vue__WEBPACK_IMPORTED_MODULE_3__.createVNode)(_component_nut_searchbar, {
    modelValue: _ctx.searchValue,
    "onUpdate:modelValue": _cache[1] || (_cache[1] = function ($event) {
      return _ctx.searchValue = $event;
    }),
    onClear: $setup.SearchClear
  }, {
    rightout: (0,vue__WEBPACK_IMPORTED_MODULE_3__.withCtx)(function () {
      return [(0,vue__WEBPACK_IMPORTED_MODULE_3__.createElementVNode)("p", {
        onClick: _cache[0] || (_cache[0] = function () {
          return $setup.SearchClick && $setup.SearchClick.apply($setup, arguments);
        })
      }, "搜索")];
    }),
    _: 1 /* STABLE */
  }, 8 /* PROPS */, ["modelValue", "onClear"]), _ctx.searchResultList.length ? ((0,vue__WEBPACK_IMPORTED_MODULE_3__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_3__.createElementBlock)("div", _hoisted_2, [((0,vue__WEBPACK_IMPORTED_MODULE_3__.openBlock)(true), (0,vue__WEBPACK_IMPORTED_MODULE_3__.createElementBlock)(vue__WEBPACK_IMPORTED_MODULE_3__.Fragment, null, (0,vue__WEBPACK_IMPORTED_MODULE_3__.renderList)(_ctx.searchResultList, function (item, idx) {
    return (0,vue__WEBPACK_IMPORTED_MODULE_3__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_3__.createElementBlock)("div", {
      key: idx,
      class: "goods",
      onClick: function onClick($event) {
        return $setup.GoGoodsDetails(item);
      }
    }, [(0,vue__WEBPACK_IMPORTED_MODULE_3__.createElementVNode)("div", _hoisted_4, [(0,vue__WEBPACK_IMPORTED_MODULE_3__.createElementVNode)("Image", {
      class: "goods-photo-img",
      src: item === null || item === void 0 ? void 0 : item.bannerImgs[0]
    }, null, 8 /* PROPS */, _hoisted_5)]), (0,vue__WEBPACK_IMPORTED_MODULE_3__.createElementVNode)("div", _hoisted_6, [(0,vue__WEBPACK_IMPORTED_MODULE_3__.createElementVNode)("p", null, (0,vue__WEBPACK_IMPORTED_MODULE_3__.toDisplayString)(item.name), 1 /* TEXT */)]), (0,vue__WEBPACK_IMPORTED_MODULE_3__.createElementVNode)("div", _hoisted_7, [(0,vue__WEBPACK_IMPORTED_MODULE_3__.createVNode)(_component_nut_price, {
      price: item.price,
      "decimal-digits": 2,
      thousands: ""
    }, null, 8 /* PROPS */, ["price"])])], 8 /* PROPS */, _hoisted_3);
  }), 128 /* KEYED_FRAGMENT */))])) : ((0,vue__WEBPACK_IMPORTED_MODULE_3__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_3__.createBlock)(_component_nut_empty, {
    key: 1,
    description: "无数据"
  }))]);
}

/***/ }),

/***/ "./src/pages/SearchGoods/index.vue":
/*!*****************************************!*\
  !*** ./src/pages/SearchGoods/index.vue ***!
  \*****************************************/
/***/ (function(__unused_webpack_module, __unused_webpack___webpack_exports__, __webpack_require__) {

/* harmony import */ var _tarojs_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @tarojs/runtime */ "./node_modules/@tarojs/runtime/dist/runtime.esm.js");
/* harmony import */ var _node_modules_tarojs_taro_loader_lib_raw_js_index_vue__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../node_modules/@tarojs/taro-loader/lib/raw.js!./index.vue */ "./node_modules/@tarojs/taro-loader/lib/raw.js!./src/pages/SearchGoods/index.vue");


var config = {"navigationBarTitleText":"搜索"};


var inst = Page((0,_tarojs_runtime__WEBPACK_IMPORTED_MODULE_0__.createPageConfig)(_node_modules_tarojs_taro_loader_lib_raw_js_index_vue__WEBPACK_IMPORTED_MODULE_1__["default"], 'pages/SearchGoods/index', {root:{cn:[]}}, config || {}))


/* unused harmony default export */ var __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_tarojs_taro_loader_lib_raw_js_index_vue__WEBPACK_IMPORTED_MODULE_1__["default"]);


/***/ }),

/***/ "./src/pages/SearchGoods/index.vue?vue&type=script&lang=js":
/*!*****************************************************************!*\
  !*** ./src/pages/SearchGoods/index.vue?vue&type=script&lang=js ***!
  \*****************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": function() { return /* reexport safe */ _node_modules_unplugin_dist_webpack_loaders_transform_js_unpluginName_unplugin_vue_components_node_modules_babel_loader_lib_index_js_clonedRuleSet_11_use_0_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_index_vue_vue_type_script_lang_js__WEBPACK_IMPORTED_MODULE_0__["default"]; }
/* harmony export */ });
/* harmony import */ var _node_modules_unplugin_dist_webpack_loaders_transform_js_unpluginName_unplugin_vue_components_node_modules_babel_loader_lib_index_js_clonedRuleSet_11_use_0_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_index_vue_vue_type_script_lang_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/unplugin/dist/webpack/loaders/transform.js?unpluginName=unplugin-vue-components!../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-11.use[0]!../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./index.vue?vue&type=script&lang=js */ "./node_modules/unplugin/dist/webpack/loaders/transform.js?unpluginName=unplugin-vue-components!./node_modules/babel-loader/lib/index.js??clonedRuleSet-11.use[0]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/SearchGoods/index.vue?vue&type=script&lang=js");
/* unplugin-vue-components disabled */ 

/***/ }),

/***/ "./src/pages/SearchGoods/index.vue?vue&type=template&id=433b3f9d":
/*!***********************************************************************!*\
  !*** ./src/pages/SearchGoods/index.vue?vue&type=template&id=433b3f9d ***!
  \***********************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": function() { return /* reexport safe */ _node_modules_unplugin_dist_webpack_loaders_transform_js_unpluginName_unplugin_vue_components_node_modules_babel_loader_lib_index_js_clonedRuleSet_11_use_0_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_3_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_index_vue_vue_type_template_id_433b3f9d__WEBPACK_IMPORTED_MODULE_0__.render; }
/* harmony export */ });
/* harmony import */ var _node_modules_unplugin_dist_webpack_loaders_transform_js_unpluginName_unplugin_vue_components_node_modules_babel_loader_lib_index_js_clonedRuleSet_11_use_0_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_3_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_index_vue_vue_type_template_id_433b3f9d__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/unplugin/dist/webpack/loaders/transform.js?unpluginName=unplugin-vue-components!../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-11.use[0]!../../../node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[3]!../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./index.vue?vue&type=template&id=433b3f9d */ "./node_modules/unplugin/dist/webpack/loaders/transform.js?unpluginName=unplugin-vue-components!./node_modules/babel-loader/lib/index.js??clonedRuleSet-11.use[0]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[3]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/SearchGoods/index.vue?vue&type=template&id=433b3f9d");
/* unplugin-vue-components disabled */

/***/ })

},
/******/ function(__webpack_require__) { // webpackRuntimeModules
/******/ var __webpack_exec__ = function(moduleId) { return __webpack_require__(__webpack_require__.s = moduleId); }
/******/ __webpack_require__.O(0, ["taro","vendors","common"], function() { return __webpack_exec__("./src/pages/SearchGoods/index.vue"); });
/******/ var __webpack_exports__ = __webpack_require__.O();
/******/ }
]);
//# sourceMappingURL=index.js.map