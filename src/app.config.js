export default defineAppConfig({
  pages: [
    'pages/Index/index',
    'pages/Menu/index',
    'pages/ShoppingCart/index',
    'pages/User/index',
    'pages/GoodsDetails/index',
    'pages/GoodsCloseAnAccount/index',
    'pages/OrderList/index',
    'pages/OrderDetail/index',
    'pages/AddressList/index',
    'pages/AddressListTypeTwo/index',
    'pages/AddressEdit/index',
    'pages/IntegralList/index',
    'pages/IntegralMarket/index',
    'pages/Setting/index',
    'pages/SearchGoods/index'
  ],
  window: {
    backgroundTextStyle: 'light',
    navigationBarBackgroundColor: '#fff',
    navigationBarTitleText: 'WeChat',
    navigationBarTextStyle: 'black',
  },
  tabBar: {
    custom: true,
    "selectedColor": "#1e4681",
    // "selectedColor": "#4da9ff",
    "list": [{
      pagePath: 'pages/Index/index',
      text: "首页",
    }, {
      pagePath: 'pages/Menu/index',
      text: "菜单"
    },{
      pagePath: 'pages/ShoppingCart/index',
      text: "购物车"
    },{
      pagePath: 'pages/User/index',
      text: "我的"
    }]
  }
})
