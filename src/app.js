import { createApp } from 'vue'
import './app.scss'
import './pages/utils/font/iconfont.css'
import component from './components'// 引入公共组件

const App = createApp({
  onShow (options) {},
  // 入口组件不需要实现 render 方法，即使实现了也会被 taro 所覆盖
}).use(component)

export default App
