import NavTitle from './NavTitle/index.vue';
import MyList from './MyList/index.vue';

const COMPONENT_NAME = [
    NavTitle,
    MyList
]
export default {
    install(app) {
        //  全局注册组件
        COMPONENT_NAME.forEach(component => {
            app.component(component.name, component)
        })
    }
  }