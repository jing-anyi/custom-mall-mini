import * as req from './request.js'
//定义全局请求地址，因为我们用到的地址是https://cnodejs.org/api/v1
export const DOMAIN = 'http://162.14.64.133:8725'


// 声明获取主题首页接口地址并导出
const IndexContentReq = DOMAIN + '/tiequan/home/getHome';
export const GetIndexContent = (data) => {
    return req.doRequestAction({
        url: IndexContentReq,
        data: data
    })
}

// 搜索商品接口
const SearchGoodsReq = DOMAIN + '/tiequan/selectProduce';
export const SearchGoods = (data) => {
    return req.doRequestAction({
        url: SearchGoodsReq,
        data: data
    })
}
// 获取标签接口
const GetTagReq = DOMAIN + '/tiequan/getTag';
export const GetTagList = (data) => {
    return req.doRequestAction({
        url: GetTagReq,
        data: data
    })
}
// 菜单栏列表接口
const MenuListReq = DOMAIN + '/tiequan/getMenu';
export const GetMenuList = (data) => {
    return req.doRequestAction({
        url: MenuListReq,
        data: data
    })
}
// 获取商品接口
const GetProduceReq = DOMAIN + '/tiequan/getProduce';
export const GetProduce = (data) => {
    return req.doRequestAction({
        url: GetProduceReq,
        data: data
    })
}
// 商品详情接口
const CommodityDetailsReq = DOMAIN + '/tiequan/details';
export const GetCommodityDetails = (data) => {
    return req.doRequestAction({
        url: CommodityDetailsReq,
        data: data
    })
}
// 加入购物车接口
const AddShoppingCartReq = DOMAIN + '/tiequan/shop';
export const AddShoppingCart = (data) => {
    return req.doRequestAction({
        url: AddShoppingCartReq,
        data: data,
        method: 'POST'
    })
}
// 获取购物车接口
const ShoppingCartListReq = DOMAIN + '/tiequan/getShop';
export const GetShoppingCartList = (data) => {
    return req.doRequestAction({
        url: ShoppingCartListReq,
        data: data,
    })
}
// 获取购物车接口
const DelShoppingCartReq = DOMAIN + '/tiequan/delShop';
export const DelShoppingCart = (data) => {
    return req.doRequestAction({
        url: DelShoppingCartReq,
        method: "POST",
        data: data,
    })
}
// 物品 +/-
const ChangeShopReq = DOMAIN + '/tiequan/changeShop';
export const ChangeShop = (data) => {
    return req.doRequestAction({
        url: ChangeShopReq,
        data: data,
    })
}

// 获取地址接口
const GetAddressListReq = DOMAIN + '/tiequan/getAddress';
export const GetAddressList = (data) => {
    return req.doRequestAction({
        url: GetAddressListReq,
        data: data,
    })
}
// 添加地址接口
const AddAddressReq = DOMAIN + '/tiequan/setAddress';
export const AddAddress = (data) => {
    return req.doRequestAction({
        url: AddAddressReq,
        data: data,
        method: 'POST',
        header: {
            "content-type": 'application/json'
        }
    })
}
// 删除地址接口
const DelAddressReq = DOMAIN + '/tiequan/delAddress';
export const DelAddress = (data) => {
    return req.doRequestAction({
        url: DelAddressReq,
        data: data,
    })
}
// 获取支付参数接口
const GetPayMessageReq = DOMAIN + '/tiequan/pay';
export const GetPayMessag = (data) => {
    return req.doRequestAction({
        url: GetPayMessageReq,
        data: data,
    })
}
// 获取订单列表接口
const OrderListReq = DOMAIN + '/tiequan/getAllOrderForm';
export const GetOrderList = (data) => {
    return req.doRequestAction({
        url: OrderListReq,
        data: data,
    })
}
// 获取积分商城接口
const IntegralMarketListReq = DOMAIN + '/tiequan/getScore';
export const GetIntegralMarketList = (data) => {
    return req.doRequestAction({
        url: IntegralMarketListReq,
        data: data,
    })
}
// 积分商品详情接口
const IntegralCommodityDetailsReq = DOMAIN + '/tiequan/scoreDetails';
export const GetIntegralCommodityDetails = (data) => {
    return req.doRequestAction({
        url: IntegralCommodityDetailsReq,
        data: data
    })
}
// 积分支付接口
const IntegralPayReq = DOMAIN + '/tiequan/scorePay';
export const IntegralPay = (data) => {
    return req.doRequestAction({
        url: IntegralPayReq,
        method: "POST",
        data: data
    })
}
// 获取用户积分
const GetUserIntegralReq = DOMAIN + '/tiequan/userInfo';
export const GetUserIntegral = (data) => {
    return req.doRequestAction({
        url: GetUserIntegralReq,
        method: "POST",
        data: data
    })
}
// 获取用户积分使用情况
const GetUserIntegralUseListReq = DOMAIN + '/tiequan/getRecord';
export const GetUserIntegralUseList = (data) => {
    return req.doRequestAction({
        url: GetUserIntegralUseListReq,
        data: data
    })
}

// 联系客服的图片
const ServiceQrCodeReq = DOMAIN + '/tiequan/support/staff';
export const GetServiceQrCode = (data) => {
    return req.doRequestAction({
        url: ServiceQrCodeReq,
        data: data
    })
}
// 登录
const LoginReq = DOMAIN + '/tiequan/get_wx_access_token';
export const ReqLogin = (data) => {
    return req.doRequestAction({
        url: LoginReq,
        method: "POST",
        data: data
    })
}
